
package com.equitybankgroup.wsdls;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.6-1b01 
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "RRN", targetNamespace = "http://rrn.equitybankgroup.com/", wsdlLocation = "http://10.1.5.46:7002/RRNValidate/RRN?wsdl")
public class RRN_Service
    extends Service
{

    private final static URL RRN_WSDL_LOCATION;
    private final static WebServiceException RRN_EXCEPTION;
    private final static QName RRN_QNAME = new QName("http://rrn.equitybankgroup.com/", "RRN");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://10.1.5.46:7002/RRNValidate/RRN?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        RRN_WSDL_LOCATION = url;
        RRN_EXCEPTION = e;
    }

    public RRN_Service() {
        super(__getWsdlLocation(), RRN_QNAME);
    }

    public RRN_Service(WebServiceFeature... features) {
        super(__getWsdlLocation(), RRN_QNAME, features);
    }

    public RRN_Service(URL wsdlLocation) {
        super(wsdlLocation, RRN_QNAME);
    }

    public RRN_Service(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, RRN_QNAME, features);
    }

    public RRN_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public RRN_Service(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns RRN
     */
    @WebEndpoint(name = "RRNPort")
    public RRN getRRNPort() {
        return super.getPort(new QName("http://rrn.equitybankgroup.com/", "RRNPort"), RRN.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns RRN
     */
    @WebEndpoint(name = "RRNPort")
    public RRN getRRNPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://rrn.equitybankgroup.com/", "RRNPort"), RRN.class, features);
    }

    private static URL __getWsdlLocation() {
        if (RRN_EXCEPTION!= null) {
            throw RRN_EXCEPTION;
        }
        return RRN_WSDL_LOCATION;
    }

}
