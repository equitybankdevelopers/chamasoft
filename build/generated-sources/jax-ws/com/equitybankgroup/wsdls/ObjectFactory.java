
package com.equitybankgroup.wsdls;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.equitybankgroup.wsdls package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetWay4Details_QNAME = new QName("http://rrn.equitybankgroup.com/", "getWay4Details");
    private final static QName _GetWay4DetailsResponse_QNAME = new QName("http://rrn.equitybankgroup.com/", "getWay4DetailsResponse");
    private final static QName _GetWay4POSDetailsResponse_QNAME = new QName("http://rrn.equitybankgroup.com/", "getWay4POSDetailsResponse");
    private final static QName _GetGenericDetails_QNAME = new QName("http://rrn.equitybankgroup.com/", "getGenericDetails");
    private final static QName _GetGenericDetailsResponse_QNAME = new QName("http://rrn.equitybankgroup.com/", "getGenericDetailsResponse");
    private final static QName _GetWay4POSDetails_QNAME = new QName("http://rrn.equitybankgroup.com/", "getWay4POSDetails");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.equitybankgroup.wsdls
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetWay4POSDetails }
     * 
     */
    public GetWay4POSDetails createGetWay4POSDetails() {
        return new GetWay4POSDetails();
    }

    /**
     * Create an instance of {@link GetGenericDetailsResponse }
     * 
     */
    public GetGenericDetailsResponse createGetGenericDetailsResponse() {
        return new GetGenericDetailsResponse();
    }

    /**
     * Create an instance of {@link GetGenericDetails }
     * 
     */
    public GetGenericDetails createGetGenericDetails() {
        return new GetGenericDetails();
    }

    /**
     * Create an instance of {@link GetWay4POSDetailsResponse }
     * 
     */
    public GetWay4POSDetailsResponse createGetWay4POSDetailsResponse() {
        return new GetWay4POSDetailsResponse();
    }

    /**
     * Create an instance of {@link GetWay4DetailsResponse }
     * 
     */
    public GetWay4DetailsResponse createGetWay4DetailsResponse() {
        return new GetWay4DetailsResponse();
    }

    /**
     * Create an instance of {@link GetWay4Details }
     * 
     */
    public GetWay4Details createGetWay4Details() {
        return new GetWay4Details();
    }

    /**
     * Create an instance of {@link TxResponse }
     * 
     */
    public TxResponse createTxResponse() {
        return new TxResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWay4Details }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://rrn.equitybankgroup.com/", name = "getWay4Details")
    public JAXBElement<GetWay4Details> createGetWay4Details(GetWay4Details value) {
        return new JAXBElement<GetWay4Details>(_GetWay4Details_QNAME, GetWay4Details.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWay4DetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://rrn.equitybankgroup.com/", name = "getWay4DetailsResponse")
    public JAXBElement<GetWay4DetailsResponse> createGetWay4DetailsResponse(GetWay4DetailsResponse value) {
        return new JAXBElement<GetWay4DetailsResponse>(_GetWay4DetailsResponse_QNAME, GetWay4DetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWay4POSDetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://rrn.equitybankgroup.com/", name = "getWay4POSDetailsResponse")
    public JAXBElement<GetWay4POSDetailsResponse> createGetWay4POSDetailsResponse(GetWay4POSDetailsResponse value) {
        return new JAXBElement<GetWay4POSDetailsResponse>(_GetWay4POSDetailsResponse_QNAME, GetWay4POSDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGenericDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://rrn.equitybankgroup.com/", name = "getGenericDetails")
    public JAXBElement<GetGenericDetails> createGetGenericDetails(GetGenericDetails value) {
        return new JAXBElement<GetGenericDetails>(_GetGenericDetails_QNAME, GetGenericDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGenericDetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://rrn.equitybankgroup.com/", name = "getGenericDetailsResponse")
    public JAXBElement<GetGenericDetailsResponse> createGetGenericDetailsResponse(GetGenericDetailsResponse value) {
        return new JAXBElement<GetGenericDetailsResponse>(_GetGenericDetailsResponse_QNAME, GetGenericDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWay4POSDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://rrn.equitybankgroup.com/", name = "getWay4POSDetails")
    public JAXBElement<GetWay4POSDetails> createGetWay4POSDetails(GetWay4POSDetails value) {
        return new JAXBElement<GetWay4POSDetails>(_GetWay4POSDetails_QNAME, GetWay4POSDetails.class, null, value);
    }

}
