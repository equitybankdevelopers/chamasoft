package com.equitybankgroup.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2015-10-07T11:36:09")
@StaticMetamodel(PayValueSetting.class)
public class PayValueSetting_ { 

    public static volatile SingularAttribute<PayValueSetting, String> id;
    public static volatile SingularAttribute<PayValueSetting, String> value;
    public static volatile SingularAttribute<PayValueSetting, String> property;
    public static volatile SingularAttribute<PayValueSetting, String> type;

}