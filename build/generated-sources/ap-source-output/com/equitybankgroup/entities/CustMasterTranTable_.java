package com.equitybankgroup.entities;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2015-10-07T11:36:09")
@StaticMetamodel(CustMasterTranTable.class)
public class CustMasterTranTable_ { 

    public static volatile SingularAttribute<CustMasterTranTable, Date> tranDate;
    public static volatile SingularAttribute<CustMasterTranTable, String> initSolId;
    public static volatile SingularAttribute<CustMasterTranTable, String> type;
    public static volatile SingularAttribute<CustMasterTranTable, String> pstdUserId;
    public static volatile SingularAttribute<CustMasterTranTable, Character> partTranType;
    public static volatile SingularAttribute<CustMasterTranTable, String> acctName;
    public static volatile SingularAttribute<CustMasterTranTable, String> tranPaidBy;
    public static volatile SingularAttribute<CustMasterTranTable, String> tranSubType;
    public static volatile SingularAttribute<CustMasterTranTable, String> instrmntAlpha;
    public static volatile SingularAttribute<CustMasterTranTable, Character> status;
    public static volatile SingularAttribute<CustMasterTranTable, BigDecimal> tranAmt;
    public static volatile SingularAttribute<CustMasterTranTable, String> countryCode;
    public static volatile SingularAttribute<CustMasterTranTable, Character> tranType;
    public static volatile SingularAttribute<CustMasterTranTable, Date> entryDate;
    public static volatile SingularAttribute<CustMasterTranTable, String> tranBrnShtDesc;
    public static volatile SingularAttribute<CustMasterTranTable, String> bankCode;
    public static volatile SingularAttribute<CustMasterTranTable, Character> pstdFlg;
    public static volatile SingularAttribute<CustMasterTranTable, Date> vfdDate;
    public static volatile SingularAttribute<CustMasterTranTable, String> deliveryChannelId;
    public static volatile SingularAttribute<CustMasterTranTable, String> partTranSrlNum;
    public static volatile SingularAttribute<CustMasterTranTable, Date> pstdDate;
    public static volatile SingularAttribute<CustMasterTranTable, Date> rcreTime;
    public static volatile SingularAttribute<CustMasterTranTable, String> tranCrncyCode;
    public static volatile SingularAttribute<CustMasterTranTable, String> rcreUserId;
    public static volatile SingularAttribute<CustMasterTranTable, String> tranRctType;
    public static volatile SingularAttribute<CustMasterTranTable, String> tranTypeMemo;
    public static volatile SingularAttribute<CustMasterTranTable, String> tranAgnAccNo;
    public static volatile SingularAttribute<CustMasterTranTable, String> tranParticular;
    public static volatile SingularAttribute<CustMasterTranTable, String> remarks;
    public static volatile SingularAttribute<CustMasterTranTable, String> tranClientType;
    public static volatile SingularAttribute<CustMasterTranTable, Long> id;
    public static volatile SingularAttribute<CustMasterTranTable, String> schmCode;
    public static volatile SingularAttribute<CustMasterTranTable, String> instrmntNum;
    public static volatile SingularAttribute<CustMasterTranTable, Character> delFlg;
    public static volatile SingularAttribute<CustMasterTranTable, String> refNum;
    public static volatile SingularAttribute<CustMasterTranTable, String> brCode;
    public static volatile SingularAttribute<CustMasterTranTable, String> tranId;
    public static volatile SingularAttribute<CustMasterTranTable, String> tranSystem;
    public static volatile SingularAttribute<CustMasterTranTable, String> custId;
    public static volatile SingularAttribute<CustMasterTranTable, Date> valueDate;
    public static volatile SingularAttribute<CustMasterTranTable, String> foracid;
    public static volatile SingularAttribute<CustMasterTranTable, BigInteger> tranFmsRctNo;
    public static volatile SingularAttribute<CustMasterTranTable, Date> recTimestamp;
    public static volatile SingularAttribute<CustMasterTranTable, String> vfdUserId;
    public static volatile SingularAttribute<CustMasterTranTable, Date> instrmntDate;
    public static volatile SingularAttribute<CustMasterTranTable, String> acid;
    public static volatile SingularAttribute<CustMasterTranTable, String> payingAcctId;
    public static volatile SingularAttribute<CustMasterTranTable, BigInteger> retries;
    public static volatile SingularAttribute<CustMasterTranTable, String> entryUserId;
    public static volatile SingularAttribute<CustMasterTranTable, String> tranRmks;

}