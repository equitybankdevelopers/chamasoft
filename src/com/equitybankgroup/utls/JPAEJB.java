/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equitybankgroup.utls;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author peter.kathae
 */
public class JPAEJB {

    private static EntityManagerFactory emf, upgEmf;

    public static EntityManagerFactory getEmf() {
        if (emf != null) {
            if (!emf.isOpen()) {
                emf = Persistence.createEntityManagerFactory("shareDataPU");
            } 
        } else {
            emf = Persistence.createEntityManagerFactory("shareDataPU");
        }
        return emf;
    } 
    public static EntityManagerFactory getUpgEmf() {
        if (upgEmf != null) {
            if (!upgEmf.isOpen()) {
                upgEmf = Persistence.createEntityManagerFactory("upgPU");
            } 
        } else {
            upgEmf = Persistence.createEntityManagerFactory("upgPU");
        }
        return upgEmf;
    }
    
}