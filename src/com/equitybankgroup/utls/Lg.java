/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.equitybankgroup.utls;

import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 *
 * @author peter.kathae
 */
public class Lg {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Lg.class.getName());

    public static void storeLog(String str) {
        log.info(str);
    }

    public static void storeErrorLog(String str) {
        try {
            log.debug(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void storeErrorLog(Exception e) {
        e.printStackTrace();
        try {
            log.debug(ChamaHelpers.getErrorMsgs(e));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
