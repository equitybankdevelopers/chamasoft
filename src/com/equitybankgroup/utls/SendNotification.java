/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equitybankgroup.utls;

import com.equitybankgroup.entities.CustMasterTranTable;
import com.equitybankgroup.entities.PayValueSetting;
import com.equitybankgroup.main.Main;
import com.equitybankgroup.wsdls.TxResponse;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

/**
 *
 * @author methuselah.ngetich
 */
public class SendNotification implements Runnable {

    private CustMasterTranTable tran;
    private String url, msg;

    public SendNotification(CustMasterTranTable tran) {
        this.tran = tran;
        this.url = Main.endPointUrl;
    }

    @Override
    public void run() {
        try {
            boolean sendNotification = sendNotification();
            //Failed notification. increase the retry
            if (!sendNotification) {
                try {
                    BigInteger retryCount = tran.getRetries().add(BigInteger.ONE);
                    tran.setRetries(retryCount);
                    EntityManager em = JPAEJB.getEmf().createEntityManager();
                    try {
                        em.getTransaction().begin();
                        em.merge(tran);
                        em.getTransaction().commit();
                        if (tran.getRetries().longValue() > 10) {
                            //get the list of people to alert
                            String emailAddrresses = "";
                            try {
                                EntityManagerFactory upgEmf = JPAEJB.getUpgEmf();
                                EntityManager upgEm = upgEmf.createEntityManager();
                                try {
                                    Query q = upgEm.createNamedQuery("PayValueSetting.findByID");
                                    q.setParameter("id", "chamaemails");
                                    List<PayValueSetting> emails = q.getResultList();
                                    for (PayValueSetting setting : emails) {
                                        emailAddrresses = setting.getValue();
                                    }
                                } catch (Exception e) {
                                    Lg.storeErrorLog(e);
                                }

                                try {
                                    upgEm.close();
                                } catch (Exception e) {
                                    Lg.storeErrorLog(e);
                                }

                            } catch (Exception e) {
                                Lg.storeErrorLog(e);
                            }
                            String[] email = StringUtils.split(emailAddrresses, ";");
                            for (int t = 0; t < email.length; t++) {
                                try {
                                    Lg.storeErrorLog("Email been sent to: " + email[t]);
                                    WsdlCenter.sendEmail(email[t], "chamasoft@equitybank.co.ke",
                                            "Failed Chama notification", this.msg);
                                } catch (Exception e) {
                                    Lg.storeErrorLog(e);
                                }

                            }
                        }
                    } catch (Exception e) {
                        Lg.storeErrorLog(e);
                    }
                    em.close();
                } catch (Exception e) {
                    Lg.storeErrorLog(e);
                }
            }
        } catch (Exception e) {
            Lg.storeErrorLog(e);
        }
    }

    private boolean sendNotification() {
        String responseString = "";
        boolean sendOk = false;
        String rrn = tran.getRefNum(), phonenumber = "", debitaccount = "", debitcustname = "";
        //Get details if from channel ID pos and equitel stk
        try {
            TxResponse TranDetails = WsdlCenter.getWay4Details(tran.getRefNum(), StringUtils.trim(tran.getDeliveryChannelId()));
            if (!StringUtils.equalsIgnoreCase("Not A Valid Channel", TranDetails.getCustName())) {
                phonenumber = "" + TranDetails.getPhone();
                debitaccount = "" + TranDetails.getSrcAccount();
                debitcustname = "" + TranDetails.getCustName();
            } else {
                if (StringUtils.isNumeric(StringUtils.trim(tran.getTranRmks()))) {
                    phonenumber = "" + StringUtils.trim(tran.getTranRmks());
                }
            }
        } catch (Exception e) {
        }
        try {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("accid", tran.getForacid());
            jsonObj.put("tranid", Main.sdf2.format(tran.getEntryDate()) + StringUtils.trim(tran.getTranId()));
            jsonObj.put("refNo", StringUtils.trim(tran.getRefNum()));
            jsonObj.put("tranDate", Main.sdf.format(tran.getEntryDate()));
            jsonObj.put("tranAmount", tran.getTranAmt());
            jsonObj.put("tranCurrency", tran.getTranCrncyCode());
            jsonObj.put("phonenumber", phonenumber);
            jsonObj.put("debitaccount", debitaccount);
            jsonObj.put("debitcustname", debitcustname);

            //its cash if instrument is null otherwise cheque
            if (StringUtils.isBlank(tran.getInstrmntNum())) {
                jsonObj.put("tranType", "CASH");
            } else {
                jsonObj.put("tranType", "CHQ");
            }
            jsonObj.put("tranParticular", StringUtils.trim(tran.getTranParticular()));
            jsonObj.put("tranRemarks", StringUtils.trim(tran.getTranRmks()));
            jsonObj.put("trandrcr", tran.getPartTranType());
            this.msg = "<em>Request</em>"
                    + "<hr/>" + jsonObj.toString();
            Lg.storeLog("Request sent =: " + jsonObj.toString());
            HttpClient httpClient = HttpClientBuilder.create().build();

            try {
                HttpPost request = new HttpPost(this.url);
                StringEntity params = new StringEntity(jsonObj.toString());
                request.addHeader("content-type", "application/x-www-form-urlencoded");
                request.setEntity(params);
                HttpResponse response = httpClient.execute(request);
                HttpEntity entity = response.getEntity();
                responseString = EntityUtils.toString(entity, "UTF-8");
                Lg.storeLog("Request =: " + jsonObj.toString() + " Response =: " + responseString);
                JSONObject resp = new JSONObject(responseString);
                if ((int) resp.get("responseCode") == 0 || (int) resp.get("responseCode") == 1) {
                    sendOk = true;
                    tran.setStatus('6');
                    EntityManager em = JPAEJB.getEmf().createEntityManager();
                    try {
                        em.getTransaction().begin();
                        em.merge(tran);
                        em.getTransaction().commit();
                    } catch (Exception e) {
                        Lg.storeErrorLog(e);
                        this.msg += "Response"
                                + "<hr/>"
                                + "<pre>"
                                + responseString
                                + "</pre>"
                                + "<hr>"
                                + "Error<hr/>"
                                + "" + ChamaHelpers.getErrorMsgs(e);
                    }
                    em.close();
                }

            } catch (Exception ex) {
                this.msg += "Response"
                        + "<hr/>"
                        + "<pre>"
                        + responseString
                        + "</pre>"
                        + "<hr>"
                        + "Error<hr/>"
                        + "" + ChamaHelpers.getErrorMsgs(ex);
                Lg.storeErrorLog(ex);
            } finally {
                httpClient.getConnectionManager().shutdown();
            }

        } catch (Exception e) {
            Lg.storeErrorLog(e);
            this.msg += "Response"
                    + "<hr/>"
                    + "<pre>"
                    + responseString
                    + "</pre>"
                    + "<hr>"
                    + "Error<hr/>"
                    + "" + ChamaHelpers.getErrorMsgs(e);
        } finally {
            try {
                Main.mp.remove(tran.getId());
            } catch (Exception e) {
                Lg.storeErrorLog(e);
            }

        }
        return sendOk;
    }

}
