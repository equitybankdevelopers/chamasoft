/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equitybankgroup.utls;

import com.equitybankgroup.emailWsdl.BMailResponse;
import com.equitybankgroup.wsdls.TxResponse;

/**
 *
 * @author methuselah.ngetich
 */
public class WsdlCenter {

    public static BMailResponse sendEmail(java.lang.String toEmail, java.lang.String fromEmail, java.lang.String toSubject, java.lang.String toMessage) {
        com.equitybankgroup.emailWsdl.BulkMailerApp service = new com.equitybankgroup.emailWsdl.BulkMailerApp();
        com.equitybankgroup.emailWsdl.BulkMailer port = service.getBMailSenderService();
        return port.sendEmail(toEmail, fromEmail, toSubject, toMessage);
    }

    public static TxResponse getGenericDetails(java.lang.String channelCode, java.lang.String rrn) {
        com.equitybankgroup.wsdls.RRN_Service service = new com.equitybankgroup.wsdls.RRN_Service();
        com.equitybankgroup.wsdls.RRN port = service.getRRNPort();
        return port.getGenericDetails(rrn);
    }

    public static TxResponse getWay4Details(java.lang.String rrn, java.lang.String channel) {
        com.equitybankgroup.wsdls.RRN_Service service = new com.equitybankgroup.wsdls.RRN_Service();
        com.equitybankgroup.wsdls.RRN port = service.getRRNPort();
        return port.getWay4Details(rrn, channel);
    }

}
