/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.equitybankgroup.utls;

import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 *
 * @author methuselah.ngetich
 */
public class ChamaHelpers {
    public static String getErrorMsgs(Exception e) {
        String msg = "";
        String[] errors = ExceptionUtils.getRootCauseStackTrace(e);
        for (String errorMsg : errors) {
            msg+=errorMsg+"<br/>";
            
        }
        return msg;
    }
}
