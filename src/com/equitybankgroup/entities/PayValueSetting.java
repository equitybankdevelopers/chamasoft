/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equitybankgroup.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author methuselah.ngetich
 */
@Entity
@Table(name = "PAY_VALUESETTING")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PayValueSetting.findByID", query = "SELECT c FROM PayValueSetting c where c.id = :id")
})
public class PayValueSetting implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "ID")
    @Id
    private String id;
    @Column(name = "VALUE")
    private String value;
    @Column(name = "TYPE")
    private String type;
    @Column(name = "PROPERTY")
    private String property;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

}
