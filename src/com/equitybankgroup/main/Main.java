/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equitybankgroup.main;

import com.equitybankgroup.entities.CustMasterTranTable;
import com.equitybankgroup.utls.GetConn;
import com.equitybankgroup.utls.JPAEJB;
import com.equitybankgroup.utls.Lg;
import com.equitybankgroup.utls.SendNotification;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import org.apache.log4j.xml.DOMConfigurator;

/**
 *
 * @author methuselah.ngetich
 */
public class Main {

    public static ExecutorService service;
    public static String endPointUrl, emailList;
    public static SimpleDateFormat sdf, sdf2;
    public static ConcurrentHashMap mp = new ConcurrentHashMap();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        GetConn c = new GetConn();
        sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        sdf2 = new SimpleDateFormat("yyyyMMdd");
//        endPointUrl = "http://chamasoft.com/endpoint";
        endPointUrl = "http://146.185.141.47:80/endpoint";
        mp = new ConcurrentHashMap();
        service = Executors.newFixedThreadPool(20);
        DOMConfigurator.configure("log4j.xml");
        EntityManagerFactory emf = JPAEJB.getEmf();

        boolean runThrough = true;
        while (runThrough) {
            EntityManager em = emf.createEntityManager();
            try {
                Query qry = em.createNamedQuery("CustMasterTranTable.findByStatusAndSchemeCode");
                List<String> schCodes = new ArrayList<>();
                schCodes.add("SB132");
                schCodes.add("CA232");
                qry.setParameter("status", '0');
                qry.setParameter("schmCode", schCodes);
                qry.setParameter("retries", 10);//
                
                List<CustMasterTranTable> trans = new ArrayList<>();
                trans = qry.getResultList();
                Lg.storeLog("Found " + trans.size() + " new transactions to notify");
                
                for (CustMasterTranTable tran : trans) {
                    try {
                        /*
                         Check if allocated to thread.
                         */
                        if (!mp.containsKey(tran.getId())) {
                            try {
                                mp.put(tran.getId(), Calendar.getInstance());
                                SendNotification sd = new SendNotification(tran);
                                service.submit(sd).get(60, TimeUnit.SECONDS);
                            } catch (Exception e) {
                                Lg.storeErrorLog(e);
                            }
                        } else {
                            /*
                             Check the request that are stuck om map
                             */
                            Lg.storeLog("TranID " + tran.getId() + " already in queue");
                            try {
                                Calendar dt = (Calendar) mp.get(tran.getId());
                                int compareTo = Calendar.getInstance().compareTo(dt);
                                if (compareTo > 0) {
                                    //Remove from the map as the request is already timed out
                                    mp.remove(tran.getId());
                                    Lg.storeLog("TranID " + tran.getId() + " removed from queue. Expired from waiting list");
                                }
                            } catch (Exception e) {
                                Lg.storeErrorLog(e);
                            }
                        }

                    } catch (Exception e) {
                        Lg.storeErrorLog(e);
                    }
                }
            } catch (Exception e) {
                Lg.storeErrorLog(e);
            }
            finally{
                try {
                    em.close();
                } catch (Exception e) {
                    Lg.storeErrorLog(e);
                }
            }
            
            try {
                Thread.sleep(100000);
                Lg.storeLog("Sleeping......");
            } catch (Exception e) {
                Lg.storeErrorLog(e);
            }
        }
    }

}
